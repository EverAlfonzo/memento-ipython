tensorflow-gpu==1.3.0
numpy==1.12.1
Keras==2.0.8
scikit-learn==0.18.2
matplotlib==2.0.2
jupyter==1.0.0
notebook==5.0.0
